# Automatización TestCafe-Cucumber-Json

Este es una forma de integrar [TestCafe](https://github.com/DevExpress/testcafe) en [CucumberJS](https://github.com/cucumber/cucumber-js) leyendo los datos de prueba desde un archivo JSON.

## Versiones
| TestCafé  | CucumberJS  |
| --------- | ----------- |
|   1.1.0   |   5.1.0     |


## instalación 

1. Debes tener instalado [Node.js](https://nodejs.org/)
2. Debes clonar el repositorio en una carpeta local
3. Navega hasta la ruta principal del repositorio desde linea de comandos
4. Usa el comando `sudo npm install` para instalar las dependencias

## Ejecutando Test

### Windows
Debes ejecutar el comando `npm run test-win`

### Mac or Linux
Debes ejecutar el comando `npm run test-lin`

### Creación de Reporte
Debes ejecutar el comando `npm run html-report`

## Documentación
* [Initial Setup](https://github.com/rquellh/testcafe-cucumber/wiki/Initial-Setup)
  * [Debuging in VSCode](https://github.com/rquellh/testcafe-cucumber/wiki/Debugging-in-VSCode)
* [Using TestCafé](https://github.com/rquellh/testcafe-cucumber/wiki/Using-TestCafe)
  * [Creating your first test](https://github.com/rquellh/testcafe-cucumber/wiki/Creating-your-first-test)
  * [Selectors](https://github.com/rquellh/testcafe-cucumber/wiki/Selectors)
  * [Actions](https://github.com/rquellh/testcafe-cucumber/wiki/Actions)
  * [Assertions](https://github.com/rquellh/testcafe-cucumber/wiki/Assertions)
* [TestCafé & CucumberJS](https://github.com/rquellh/testcafe-cucumber/wiki/TestCafe-&-CucumberJS)
  * [Helpful VSCode Setup](https://github.com/rquellh/testcafe-cucumber/wiki/Helpful-VSCode-Setup)
  * [Creating Features](https://github.com/rquellh/testcafe-cucumber/wiki/Creating-Features)
  * [Creating Step Definitions](https://github.com/rquellh/testcafe-cucumber/wiki/Creating-Step-Definitions)
  * [Adding TestCafé Functionality to Cucumber steps](https://github.com/rquellh/testcafe-cucumber/wiki/Adding-TestCafe-Functionality-to-Cucumber-steps)
  * [Harnessing Cucumber's Power](https://github.com/rquellh/testcafe-cucumber/wiki/Harnessing-Cucumber's-Power)
  * [Page Object](https://github.com/rquellh/testcafe-cucumber/wiki/Page-Object)
  * [Running Tests](https://github.com/rquellh/testcafe-cucumber/wiki/Running-Tests)
  * [Reporting and Taking Screenshots](https://github.com/rquellh/testcafe-cucumber/wiki/Reporting-and-Taking-Screenshots)
  * [Reporting Multiple Cucumber HTML Reporter](https://github.com/wswebcreation/multiple-cucumber-html-reporter)