Feature: Searching for TestCafe by Google

  I want to find TestCafe repository by Google search

  Scenario Outline: Searching for TestCafe by Google
    Given I am open Google's search page
    When I am typing my search request "<Search>" on Google
    Then I press the "enter" key on Google
    Then I should see that the first Google's result is "<Result>"
    Examples:
    |        Search        |         Result        |
    | ${data.test1.search} | ${data.test1.result}  |
    | ${data.test2.search} | ${data.test2.result}  |


  Scenario Outline: Failing scenario
    Given I am open Google's search page
    When I am typing my search request "<Search>" on Google
    Then I press the "enter" key on Google
    Then I should see that the first Google's result is "<Result>"
    Examples:
    |        Search        |         Result        |
    | ${data.testFailed.search} | ${data.testFailed.result}  |