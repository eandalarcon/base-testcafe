const {Selector} = require('testcafe');

function select(selector) {
  return Selector(selector).with({boundTestRun: testController});
}

exports.PruebaPO = {
  url: function() {
      return 'https://google.com';
  },
  searchBox: function() {
      return select('.gLFyf');
  },
  firstSearchResult: function() {
      return select('#rso').find('a')
  }
};
